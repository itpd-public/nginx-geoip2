# Nginx Geoip2
if you want use geoip2 you must change config nginx.conf like that

```
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;

    keepalive_timeout  65;

    limit_req_zone $binary_remote_addr zone=backend:10m rate=20r/m;
    geoip2 /usr/share/GeoIP/GeoIP2-Country.mmdb {
        auto_reload 60m;
        $geoip2_metadata_country_build metadata build_epoch;
        $geoip2_data_country_code country iso_code;
        $geoip2_data_country_name country names en;
    }
    include /etc/nginx/conf.d/*.conf;
}
```

and copy GeoIP2-Country.mmdb to the /usr/share/GeoIP
